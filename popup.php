<?php

class ImOptinGuruPopup {
	function __construct() {
		add_action( 'wp_ajax_guru_popupSaveEmail', array(&$this, 'save_email') );
		add_action( 'wp_ajax_nopriv_guru_popupSaveEmail', array(&$this, 'save_email') );
		
		if(!is_admin()) {
			add_action( 'wp_enqueue_scripts', array(&$this, 'styles_scripts') );
			add_action( 'wp_footer', array(&$this, 'footer_script') );
			add_shortcode('optin-popup', array(&$this, 'optin_popup'));
		}
	}
	
	function save_email() {
		global $wpdb;
		if(!empty($_POST['email'])) {
			$sql = "SELECT email FROM {$wpdb->prefix}imog_emails WHERE email='{$_POST['email']}'";
			$email = $wpdb->get_var($sql);
			if($email) {
				die(json_encode(array('message'=>"Already saved.", 'error'=>'1')));
			}
			
			//save now
			$wpdb->insert("{$wpdb->prefix}imog_emails", array(
				'email' => $_POST['email']
			));
			if($wpdb->insert_id) {
				die(json_encode(array('message'=>"Successfully saved.")));
			}
		}
		die(json_encode(array('message'=>"Cant save the email.", 'error'=>'1')));
	}
	
	/* // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
	wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234 ) ); */
	
	function footer_script() {
		if ( wp_script_is( 'optin-popup', 'enqueued' ) ) { //registered enqueued / queue
			?>
			<script type="text/javascript">
				var home_url = '<?php echo site_url()?>';
				var admin_ajax_url = '<?php echo admin_url( 'admin-ajax.php' )?>';
			</script>
			<?php
		}
	}

	function styles_scripts() {
		//css
		wp_register_style( 'optin-popup', plugins_url( '/css/style.css', __FILE__ ), array(), '1', 'all' );
		wp_enqueue_style( 'optin-popup' );
		//js
		wp_register_script( 'optin-popup', plugins_url( '/js/script.js', __FILE__ ), array(), '1', true );
		wp_enqueue_script( 'optin-popup' );
	}
	
	// [optin-popup]
	function optin_popup() {
		
		$popup = '
		<div class="optin-popup">
			<div class="optin-popup-content">
				<span class="optin-popup-close">&#10006;</span>
				<form>
					<input type="hidden" name="action" value="guru_popupSaveEmail" />
					<label>Email: <br><input name="email" type="email" value="" required/></label>
					<input name="submit" type="submit" value="Submit" />
					<div class="optin-popup-message"></div>
				</form>
			</div>
		</div>
		';
		
		$button = '
		<a class="optin-button" href="javascript:;">
			Subscribe
		</a>
		';
		
		return $popup . $button;
	}
}

?>