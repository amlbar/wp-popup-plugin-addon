jQuery(document).ready(function($){
	$('body').append('<div id="optin-popup-wall"></div>');
	$('#optin-popup-wall').css({
		'position':'fixed',
		'right':'0px',
		'left':'0px',
		'bottom':'0px',
		'top':'0px',
		'z-index':'2147483600',
		'opacity':'0.5',
		'background':'black',
		'display':'none'
	});
	// $('.optin-popup').show();
	
	$('.optin-popup').each(function(){
		$(this).appendTo('body');
	});
	
	$('.optin-button').on('click', function(e){
		$('.optin-popup').show().css({'top':$('body').scrollTop()});
		$('#optin-popup-wall').show();
	});
	
	$('.optin-popup').on('click', '.optin-popup-close', function(e){
		$(this).parents('.optin-popup').hide();
		$('#optin-popup-wall').hide();
	});
	
	$('.optin-popup form').submit(function(e){
		var data = $(this).serialize();
		var div_message = $(this).find('.optin-popup-message');
		var message = 'Something went wrong..';
		
		var loader = $( '<img/>', { src: home_url + '/wp-admin/images/loading.gif', 'class': 'loader-image' });
		div_message.html( loader ).removeClass('success');
		
		jQuery.post(admin_ajax_url, data, function(response) {
			// response = JSON.parse(response);
			console.log(response);
			if(response.error){
				div_message.html('');
				if(response.message) message = response.message;
				alert(message);
			}
			else{
				div_message.html(response.message).addClass('success');
			}
			
		}, 'json').fail(function(){
			div_message.html('');
		});
		
		return false;
	});
});
